<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTableGamePrizesList extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gamePrizeList', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('gamePrizeId');
            $table->text('coin');
            $table->text('coinPhotoWeb')->nullable();
            $table->text('coinPhotoMob')->nullable();
            $table->text('coinPhotoMobRedeem')->nullable();
            $table->text('coinPhotoWebRedeem')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gamePrizeList');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Ads extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('affinity', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('brand');
            $table->string('slug');
            $table->text('brandImage')->nullable();
            $table->text('coinText');
            $table->text('coinBrand')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('affinity');
    }
}

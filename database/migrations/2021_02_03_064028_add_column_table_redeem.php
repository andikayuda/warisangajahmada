<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnTableRedeem extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('redeem', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('kodeRedeem');
            $table->string('nama');
            $table->string('noHandphone');
            $table->string('jenisHadiah');
            $table->string('tipeHadiah');
            $table->string('email',100)->unique();
            $table->string('noWhatsapp',50);
            $table->text('alamat');
            $table->string('pilihanPembayaran', 50)->nullable();
            $table->string('alamatPembayaran')->nullable();
            $table->string('statusRedeem')->nullable();
            $table->dateTime('tanggalStatusRedeem',0)->nullable();
            $table->string('statusKoin')->nullable();
            $table->dateTime('tanggalStatusKoin',0)->nullable();
            $table->string('statusHadiah')->nullable();
            $table->dateTime('tanggalStatusHadiah',0)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('redeem');
    }
}

@extends('layouts.app')

@section('content')
    <section class="container-fluid ads-header-wrapper">
      <div class="ads-header">
        <div class="ads-header-img">
          <img src="{{asset($data->brandImage)}}" alt="">
        </div>
        <h1>
          {{$data->title}}
        </h1>
        <h6>
          Ayo kumpulkan Koin Gatotkaca sebanyak-banyaknya! 
          <br>
          {{$data->brand}} bisa jadi milikmu!
        </h6>
      </div>
        <div class="content">
          <span>Cara</span>
          <span>Mendapatkan</span>
          <div class="container step1">
            <div class="row">
              <div class="col-md-4 prize-image-wrapper">
                <img src="{{URL::asset('images/prize_step_1.png')}}" alt="" class="prize-step-image1">
              </div>
              {{-- <div class="col-1 prize-step-numbers">
                <h1>1.</h1>
              </div> --}}
              <div class="col-md-8" >
                <div class="row prize-step-1 ads-step-1">
                  <div class="col-1 prize-step-numbers">
                    <h1>1.</h1>
                  </div>
                  <div class="col-10 prize_step_text">
                    <span>Beli</span>
                    <span>Larutan Penyegar</span>
                    <br>
                    <span>Cap Badak</span>
                    <p>Beli paket Bundle isi 6 pcs kaleng Larutan Penyegar Cap Badak yang sudah berisi Koin Gatotkaca.</p>
                    <a class="btn btn-danger" href="{{route('home')}}" role="button">PESAN SEKARANG</a> 
                  </div>
                </div>                       
              </div>
            </div>
          </div>
          <div class="container step2 ads-step-2">
            <div class="row prize-step-2 ads-step-2">
              <div class="col-1">
                <h1 class="number-step-1">2.</h1>
              </div>
              <div class="col-11 prize_step_text-2">
                <span>Kumpulkan</span>
                <span>Koin Gatotkaca</span>
                <p>{{$data->coinText}}</p>
              </div>
              <div class="img-ads-wrapper">
              @if (SESSION::get('isMobile'))
              <img src="{{asset($data->mobileCoin)}}" alt="" class="img-ads-mobile">
              @else   
                <img src="{{asset($data->coinBrand)}}" alt="" class="img-ads-web">
              @endif
              </div>
            </div>
          </div>
          <div class="container step3">
            <div class="row ads-step-3">
              <div class="col-md-8 step3-text">
                <div class="row">
                  <div class="col-1 prize-step-numbers-3">
                    <h1>3.</h1>
                  </div>
                  <div class="col-7 prize_step_text-3">
                    <span>Tukarkan</span>
                    <span>Koin Gatotkaca</span>
                    <p>Setelah kamu mengumpulkan semua koinnya, tukarkan koinmu disini</p>
                    <a class="btn btn-danger" href="{{route('redeem')}}" role="button">TUKAR SEKARANG</a>            
                  </div>
                </div>
              </div>
              <div class="col-md-4 img-step-3-wrapper">
                <img src="{{URL::asset('images/step_3.png')}}" alt="">
              </div>
            </div>
          </div>
        </div>
      </div>
        <div class="ads-penawaran-wrapper">
          <h1>Lihat Penawaran Lainnya</h1>
          <div class="swiper-container">
            <div class="swiper-wrapper">
              <div class="swiper-slide">
                <a href="">
                  <div class="ads-img-slide">
                    <img src="{{URL::asset('images/iphone.png')}}" alt="">
                  </div>
                  <h3>Iphone</h3>
                </a>
              </div>
              <div class="swiper-slide">
                <a href="">
                  <div class="ads-img-slide">
                    <img src="{{URL::asset('images/ps5.png')}}" alt="">
                  </div>
                  <h3>Ps5</h3>
                </a>
              </div>
              <div class="swiper-slide">
                <a href="">
                  <div class="ads-img-slide">
                    <img src="{{URL::asset('images/bike.png')}}" alt="">
                  </div>
                  <h3>Sepeda</h3>
                </a>
              </div>
              <div class="swiper-slide">
                <a href="">
                  <div class="ads-img-slide">
                    <img src="{{URL::asset('images/cell_operator.png')}}" alt="">
                  </div>
                  <h3>Pulsa 200k</h3>
                </a>
              </div>
              <div class="swiper-slide">
                <a href="">
                  <div class="ads-img-slide">
                    <img src="{{URL::asset('images/FF.png')}}" alt="">
                  </div>
                  <h3>Diamond FF</h3>
                </a>
              </div>
              <div class="swiper-slide">
                <a href="">
                  <div class="ads-img-slide">
                    <img src="{{URL::asset('images/ML.png')}}" alt="">
                  </div>
                  <h3>Diamond ML</h3>
                </a>
              </div>
              <div class="swiper-slide">
                <a href="">
                  <div class="ads-img-slide">
                    <img src="{{URL::asset('images/TOKPED.png')}}" alt="">
                  </div>
                  <h3>Voucher Belanja</h3>
                </a>
              </div>
            </div>
          </div>
          <div class="swiper-pagination swiper-pagination-black"></div>
        </div>
    </section>
@endsection

@section('js')
<script src="{{ URL::asset('js/swiper-bundle.min.js') }}"></script>

<script>
  var swiper = new Swiper('.swiper-container', {
    slidesPerView: 3,
    spaceBetween: 30,
    pagination: {
      el: '.swiper-pagination',
      clickable: true,
    },
    // swiper.on()
  });
</script>
@endsection
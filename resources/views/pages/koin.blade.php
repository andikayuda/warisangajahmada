@extends('layouts.app')

@section('content')
    <div>
        {{-- <div class="hero-koin">
            <h1>Koin Gatotkaca <br> eksklusif di Warisan Gajahmada</h1>
        </div> --}}

        <div class="hero-koin">
            <div class="marquee-koin">
                <div class="hero-txt">
                    <h1>Koin Gatotkaca <br> eksklusif di Warisan Gajahmada</h1>
                </div>
                <div class="track-koin">
                    <img class="marquee-img-koin" src="{{ url('./images/marquee/koin-banner.png') }}" alt="">
                    <img class="marquee-img-koin" src="{{ url('./images/marquee/koin-banner.png') }}" alt="">
                </div>
            </div>
        </div>



        <div class="koin-desc">
            <h1><span>Koin</span> Gatotkaca</h1>
            <p>Satria Dewa Gatotkaca dan Cap Badak mempersembahkan 10 koleksi Koin
                Misteri Gatotkaca yang didesain sesuai atribut khas Gatotkaca dengan
                material terbaik.</p>

            {{-- <img src="{{ url('./images/koin.png') }}" alt="" class="koin-img img-fluid"> --}}
            <div class="koin-display"></div>

            
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <img src="{{ url('./images/how-to-koin.png') }}" alt="" class="img-fluid">
                    </div>
                    <div class="col-md-6 koin-cara">
                        <h1><span>Cara</span> Mendapatkan</h1>
                        <p>Hanya beli paket Bundle isi 6 pcs kaleng Larutan Penyegar Cap Badak yang sudah berisi Koin Gatotkaca, Anda bisa menangi hadiahnya!</p>
                        <button>Pesan Sekarang</button>
                    </div>
                </div>
            </div>

            <div class="koin-fungsi">
                <h1>Fungsi Koin Gatotkaca</h1>
                <div class="container">
                    <div class="row" style="row-gap: 25px; margin-top: 50px">

                        <div class="col-6 col-md-3">
                            <div class="card koin-card">
                                <div class="card-body">
                                    <img src="{{ url('./images/collectible.svg') }}" alt="" class="img-fluid fungsi-img">
                                    <h5>Collectible Item</h5>
                                    <p>Bagi Anda yang hobi mengoleksi barang unik, 10 jenis Koin Gatotkaca yang kece ini udah pasti gak boleh sampai ketinggalan!</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-6 col-md-3">
                            <div class="card koin-card">
                                <div class="card-body">
                                    <img src="{{ url('./images/exchange.svg') }}" alt="" class="img-fluid fungsi-img">
                                    <h5>Ditukar Hadiah Uang</h5>
                                    <p>Setiap jenis Koin Gatotkaca bisa Anda tukar dengan hadiah ratusan hingga jutaan rupiah.</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-6 col-md-3">
                            <div class="card koin-card">
                                <div class="card-body">
                                    <img src="{{ url('./images/collector.svg') }}" alt="" class="img-fluid fungsi-img">
                                    <h5>Ditukar Collector Kit</h5>
                                    <p>Collector Kit Koin Gatotkaca yang mewah dan Limited Edition juga bisa Anda bawa pulang kok!</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-6 col-md-3">
                            <div class="card koin-card">
                                <div class="card-body">
                                    <img src="{{ url('./images/meet.svg') }}" alt="" class="img-fluid fungsi-img">
                                    <h5>Bertemu Cast Gatotkaca</h5>
                                    <p>Kesempatan emas untuk bertemu secara online dengan cast Film Satria Dewa Gatotkaca gak boleh Anda sia-siakan!</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@extends('layouts.app')

@section('content')
    <section class="container-fluid">
      <div class="prize-header">
        <div class="prize-header-img-wrapper">
          <img src="{{URL::asset('images/koin_prize.png')}}" alt="">
        </div>
        <p>
          Ayo buruan beli Larutan Penyegar Cap Badak di
          <br> 
          Warisan Gajahmada dan raih hadiahnya!</p>
      </div>
      <div class="content">
        <span>Cara</span>
        <span>Mendapatkan</span>
        <div class="container step1 ">
          <div class="row">
            <div class="col-md-4 prize-image-wrapper">
              <img src="{{URL::asset('images/prize_step_1.png')}}" alt="" class="prize-step-image1">
            </div>
            {{-- <div class="col-1 prize-step-numbers">
              <h1>1.</h1>
            </div> --}}
            <div class="col-md-8" >
              <div class="row prize-step-1">
                <div class="col-1 prize-step-numbers">
                  <h1>1.</h1>
                </div>
                <div class="col-10 prize_step_text">
                  <span>Beli</span>
                  <span>Larutan Penyegar</span>
                  <br>
                  <span>Cap Badak</span>
                  <p>Beli paket Bundle isi 6 pcs kaleng Larutan Penyegar Cap Badak yang sudah berisi Koin Gatotkaca.</p>
                  <a class="btn btn-danger" href="{{route('home')}}" role="button">PESAN SEKARANG</a> 
                </div>
              </div>                       
            </div>
          </div>
        </div>
        <div class="container step2">
          <div class="row prize-step-2">
            <div class="col-1">
              <h1 class="number-step-1">2.</h1>
            </div>
            <div class="col-11 prize_step_text-2">
              <span>Kumpulkan</span>
              <span>Koin Gatotkaca</span>
              <p>Kumpulkan 3 buah Koin Gatotkaca Premium, 3 Koin Gatotkaca Emas, 3 Koin Gatotkaca Regular, dan 3 Koin Gatotkaca</p>
            </div>
          </div>
        </div>
        <div class="prize-item-card">
          <h2>1 item</h2>
          <div class="row">
            <div class="col-md-3 col-6">
              <div class="grid-inside">
                  <img src="{{URL::asset('images/iphone.png')}}" alt="iphone" class="prize-iphone">
                  <img src="{{URL::asset('images/iphone_koin.png')}}" alt="iphone_koin" class="prize-iphone_koin">
                <button type="button" class="btn btn-prize-detail" id="iphoneButton" data-toggle="modal" data-target="#koin-modal">Lihat Detail</button>
              </div>
              </div>
            <div class="col-md-3 col-6">
              <div class="grid-inside">
                <img src="{{URL::asset('images/ps5.png')}}" alt="ps5" class="prize-ps5">
                <img src="{{URL::asset('images/ps5_koin.png')}}" alt="ps5_koin" class="prize-ps5_koin"> 
                <button type="button" class="btn btn-prize-detail" id="ps5Button" data-toggle="modal" data-target="#koin-modal-ps5">Lihat Detail</button>
              </div>
            </div>
            <div class="col-md-3 col-6">
              <div class="grid-inside">
                <img src="{{URL::asset('images/bike.png')}}" alt="sepeda" class="prize-bike">
                <img src="{{URL::asset('images/bike_koin.png')}}" alt="bike_koin" class="prize-bike_koin"> 
                <button type="button" class="btn btn-prize-detail" id="bikeButton" data-toggle="modal" data-target="#koin-modal-sepeda">Lihat Detail</button>
              </div>
            </div>
            <div class="col-md-3 col-6">
              <div class="grid-inside">
                <img src="{{URL::asset('images/cell_operator.png')}}" alt="operator seluler" class="prize-cell_operator">
                <img src="{{URL::asset('images/pulsa_koin.png')}}" alt="pulsa_koin" class="prize-pulsa_koin"> 
                <button type="button" class="btn btn-prize-detail" id="cellButton" data-toggle="modal" data-target="#koin-modal-voucher">Lihat Detail</button>
              </div>
            </div>
          </div>
        </div>
        <div class="prize-game-cash">
          <h2>Game Cash</h2>
          <div class="row">
            <div class="col-md-3 col-6">
            <div class="grid-inside-game">
              <img src="{{URL::asset('images/FF.png')}}" alt="iphone" class="prize-free-fire">
            <button type="button" class="btn btn-prize-detail-game" id="free-fireButton" data-toggle="modal" data-target="#koin-modal-ff">Lihat Detail</button>
          </div>
          </div>
        <div class="col-md-3 col-6">
          <div class="grid-inside-game">
            <img src="{{URL::asset('images/ML.png')}}" alt="ps5" class="prize-mole">
            <button type="button" class="btn btn-prize-detail-game" id="moleButton" data-toggle="modal" data-target="#koin-modal-ml">Lihat Detail</button>
          </div>
        </div>
        <div class="col-md-3 col-6">
          <div class="grid-inside-game">
            <img src="{{URL::asset('images/TOKPED.png')}}" alt="sepeda" class="prize-tokped">
            <button type="button" class="btn btn-prize-detail-game" id="tokpedButton" data-toggle="modal" data-target="#koin-modal-tokped">Lihat Detail</button>
          </div>
        </div>
            <div class="col-md-3 col-6"></div>
          </div>
        </div>
        <div class="game-cash-prize">
          {{-- table FF --}}
          <table class="table-koin-redeem">
            <thead>
              <tr>
                <th scope="col" class="table-title-1">Jumlah Diamond</th>
                <th scope="col" class="table-title-2">Koin yang dibutuhkan</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>Diamond FF 1jt</td>
                <td><img src="{{URL::asset('images/redeem_ff1jt.png')}}" alt="" class="prize-image-table"></td>
              </tr>
              <tr>
                <td>Diamond FF 500rb</td>
                <td><img src="{{URL::asset('images/redeem_ff500rb.png')}}" alt=""  class="prize-image-table"></td>
              </tr>
              <tr>
                <td>Diamond FF 300rb</td>
                <td><img src="{{URL::asset('images/redeem_ff300rb.png')}}" alt=""  class="prize-image-table"></td>
              </tr>
              <tr>
                <td>Diamond FF 200rb</td>
                <td><img src="{{URL::asset('images/redeem_ff200rb.png')}}" alt=""  class="prize-image-table"></td>
              </tr>
              <tr>
                <td>Diamond FF 100rb</td>
                <td><img src="{{URL::asset('images/redeem_ff100rb.png')}}" alt=""  class="prize-image-table-1"></td>
              </tr>
              <tr>
                <td>Diamond FF 50rb</td>
                <td><img src="{{URL::asset('images/redeem_ff50rb.png')}}" alt=""  class="prize-image-table-3"></td>
              </tr>
              <tr>
                <td>Diamond FF 20rb</td>
                <td><img src="{{URL::asset('images/redeem_ff20rb.png')}}" alt=""  class="prize-image-table-2"></td>
              </tr>
            </tbody>
          </table>

          {{-- table mole --}}
          <table class="table-koin-redeem-1">
            <thead>
              <tr>
                <th scope="col" class="table-title-1">Jumlah Diamond</th>
                <th scope="col" class="table-title-2">Koin yang dibutuhkan</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>Diamond ML 1jt</td>
                <td><img src="{{URL::asset('images/redeem_ml1jt.png')}}" alt="" class="prize-image-table"></td>
              </tr>
              <tr>
                <td>Diamond ML 500rb</td>
                <td><img src="{{URL::asset('images/redeem_ml500rb.png')}}" alt=""  class="prize-image-table"></td>
              </tr>
              <tr>
                <td>Diamond ML 300rb</td>
                <td><img src="{{URL::asset('images/redeem_ml300rb.png')}}" alt=""  class="prize-image-table-3"></td>
              </tr>
              <tr>
                <td>Diamond ML 165rb</td>
                <td><img src="{{URL::asset('images/redeem_ml165rb.png')}}" alt=""  class="prize-image-table-6"></td>
              </tr>
              <tr>
                <td>Diamond ML 120rb</td>
                <td><img src="{{URL::asset('images/redeem_ml120rb.png')}}" alt=""  class="prize-image-table-5"></td>
              </tr>
              <tr>
                <td>Diamond ML 88rb</td>
                <td><img src="{{URL::asset('images/redeem_ml88rb.png')}}" alt=""  class="prize-image-table-1"></td>
              </tr>
              <tr>
                <td>Diamond ML 71,5rb</td>
                <td><img src="{{URL::asset('images/redeem_ml71,5rb.png')}}" alt=""  class="prize-image-table"></td>
              </tr>
              <tr>
                <td>Diamond ML 49rb</td>
                <td><img src="{{URL::asset('images/redeem_ml49rb.png')}}" alt=""  class="prize-image-table-4"></td>
              </tr>
            </tbody>
          </table>

          {{-- table tokped --}}
          <table class="table-koin-redeem-2">
            <thead>
              <tr>
                <th scope="col" class="table-title-1">Jumlah Voucher Belanja</th>
                <th scope="col" class="table-title-2">Koin yang dibutuhkan</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>Voucher Belanja 1jt</td>
                <td><img src="{{URL::asset('images/redeem_tokped1jt.png')}}" alt="" class="prize-image-table"></td>
              </tr>
              <tr>
                <td>Voucher Belanja 500rb</td>
                <td><img src="{{URL::asset('images/redeem_tokped500rb.png')}}" alt=""  class="prize-image-table"></td>
              </tr>
              <tr>
                <td>Voucher Belanja 250rb</td>
                <td><img src="{{URL::asset('images/redeem_tokped250rb.png')}}" alt=""  class="prize-image-table"></td>
              </tr>
              <tr>
                <td>Voucher Belanja 150rb</td>
                <td><img src="{{URL::asset('images/redeem_tokped150rb.png')}}" alt=""  class="prize-image-table"></td>
              </tr>
            </tbody>
          </table>
          {{-- <span>Jumlah Diamond</span>
          <span>Koin yang dibutuhkan</span> --}}
          {{-- <img src="{{URL::asset('images/free-fire-1.png')}}" alt=""> --}}
        </div>
        <div class="container step3">
          <div class="row">
            <div class="col-md-8 step3-text">
              <div class="row">
                <div class="col-1 prize-step-numbers-3">
                  <h1>3.</h1>
                </div>
                <div class="col-7 prize_step_text-3">
                  <span>Tukarkan</span>
                  <span>Koin Gatotkaca</span>
                  <p>Setelah kamu mengumpulkan semua koinnya, tukarkan koinmu disini</p>
                  <a class="btn btn-danger" href="{{route('redeem')}}" role="button">TUKAR SEKARANG</a>            
                </div>
              </div>
            </div>
            <div class="col-md-4 img-step-3-wrapper">
              <img src="{{URL::asset('images/step_3.png')}}" alt="">
            </div>
          </div>
        </div>
      </div>

      {{-- modal iphone--}}
      <div class="modal" id="koin-modal" tabindex="-1">
        <div class= "modal-dialog modal-dialog-centered" >
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <h1>Koin yang harus ditukarkan</h1>
              <img src="{{URL::asset('images/prize-mobile/iphone_koin.png')}}" alt="iphone_koin" class="prize-mobile-iphone_koin">
            </div>
          </div>
        </div>
      </div>
      {{-- modal ps5 --}}
      <div class="modal" id="koin-modal-ps5" tabindex="-1">
        <div class= "modal-dialog modal-dialog-centered" >
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <h1>Koin yang harus ditukarkan</h1>
              <img src="{{URL::asset('images/prize-mobile/ps5_koin.png')}}" alt="ps5_koin" class="prize-mobile-ps5_koin"> 
            </div>
          </div>
        </div>
      </div>
      {{-- modal sepeda--}}
      <div class="modal" id="koin-modal-sepeda" tabindex="-1">
        <div class= "modal-dialog modal-dialog-centered" >
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <h1>Koin yang harus ditukarkan</h1>
              <img src="{{URL::asset('images/prize-mobile/bike_koin.png')}}" alt="bike_koin" class="prize-mobile-bike_koin"> 
            </div>
          </div>
        </div>
      </div>
      {{-- modal voucher--}}
      <div class="modal" id="koin-modal-voucher" tabindex="-1">
        <div class= "modal-dialog modal-dialog-centered" >
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <h1>Koin yang harus ditukarkan</h1>
              <img src="{{URL::asset('images/prize-mobile/pulsa_koin.png')}}" alt="pulsa_koin" class="prize-mobile-pulsa_koin"> 
            </div>
          </div>
        </div>
      </div>
      
      {{-- modal ff--}}
      <div class="modal" id="koin-modal-ff" tabindex="-1">
        <div class= "modal-dialog modal-dialog-centered" >
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <h1>Koin yang harus ditukarkan</h1>
              <div class="swiper-modal-container">
                <div class="swiper-wrapper">
                  <div class="swiper-slide swiper-no-border">
                    <h1>Diamond FF <b>1jt</b></h1>
                    <div>
                      <img src="{{URL::asset('images/prize-mobile/redeem_mobile_ff1jt.png')}}" alt="pulsa_koin" class="prize-mobile-pulsa_koin"> 
                    </div>
                  </div>
                  <div class="swiper-slide swiper-no-border">
                    <h1>Diamond FF <b>500rb</b></h1>
                    <div>
                      <img src="{{URL::asset('images/prize-mobile/redeem_mobile_ff500rb.png')}}" alt="pulsa_koin" class="prize-mobile-pulsa_koin"> 
                    </div>
                  </div>
                  <div class="swiper-slide swiper-no-border">
                    <h1>Diamond FF <b>300rb</b></h1>
                    <div>
                      <img src="{{URL::asset('images/prize-mobile/redeem_mobile_ff300rb.png')}}" alt="pulsa_koin" class="prize-mobile-pulsa_koin"> 
                    </div>
                  </div>
                  <div class="swiper-slide swiper-no-border">
                    <h1>Diamond FF <b>200rb</b></h1>
                    <div>
                      <img src="{{URL::asset('images/prize-mobile/redeem_mobile_ff200rb.png')}}" alt="pulsa_koin" class="prize-mobile-pulsa_koin"> 
                    </div>
                  </div>
                  <div class="swiper-slide swiper-no-border">
                    <h1>Diamond FF <b>100rb</b></h1>
                    <div>
                      <img src="{{URL::asset('images/prize-mobile/redeem_mobile_ff100rb.png')}}" alt="pulsa_koin" class="prize-mobile-pulsa_koin"> 
                    </div>
                  </div>
                  <div class="swiper-slide swiper-no-border">
                    <h1>Diamond FF <b>50rb</b></h1>
                    <div>
                      <img src="{{URL::asset('images/prize-mobile/redeem_mobile_ff50rb.png')}}" alt="pulsa_koin" class="prize-mobile-pulsa_koin"> 
                    </div>
                  </div>
                  <div class="swiper-slide swiper-no-border">
                    <h1>Diamond FF <b>20rb</b></h1>
                    <div>
                      <img src="{{URL::asset('images/prize-mobile/redeem_mobile_ff20rb.png')}}" alt="pulsa_koin" class="prize-mobile-pulsa_koin"> 
                    </div>
                  </div>
                </div>
                <div class="swiper-button-next"></div>
                <div class="swiper-button-prev"></div>
              </div>
            </div>
          </div>
        </div>
      </div>

      {{-- modal mole--}}
      <div class="modal" id="koin-modal-ml" tabindex="-1">
        <div class= "modal-dialog modal-dialog-centered" >
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <h1>Koin yang harus ditukarkan</h1>
              <div class="swiper-modal-container">
                <div class="swiper-wrapper">
                  <div class="swiper-slide swiper-no-border">
                    <h1>Diamond ML <b>1jt</b></h1>
                    <div>
                      <img src="{{URL::asset('images/prize-mobile/redeem_mobile_ml1jt.png')}}" alt="pulsa_koin" class="prize-mobile-pulsa_koin"> 
                    </div>
                  </div>
                  <div class="swiper-slide swiper-no-border">
                    <h1>Diamond ML <b>500rb</b></h1>
                    <div>
                      <img src="{{URL::asset('images/prize-mobile/redeem_mobile_ml500rb.png')}}" alt="pulsa_koin" class="prize-mobile-pulsa_koin"> 
                    </div>
                  </div>
                  <div class="swiper-slide swiper-no-border">
                    <h1>Diamond ML <b>300rb</b></h1>
                    <div>
                      <img src="{{URL::asset('images/prize-mobile/redeem_mobile_ml300rb.png')}}" alt="pulsa_koin" class="prize-mobile-pulsa_koin"> 
                    </div>
                  </div>
                  <div class="swiper-slide swiper-no-border">
                    <h1>Diamond ML <b>165rb</b></h1>
                    <div>
                      <img src="{{URL::asset('images/prize-mobile/redeem_mobile_ml165rb.png')}}" alt="pulsa_koin" class="prize-mobile-pulsa_koin"> 
                    </div>
                  </div>
                  <div class="swiper-slide swiper-no-border">
                    <h1>Diamond ML <b>120rb</b></h1>
                    <div>
                      <img src="{{URL::asset('images/prize-mobile/redeem_mobile_ml120rb.png')}}" alt="pulsa_koin" class="prize-mobile-pulsa_koin"> 
                    </div>
                  </div>
                  <div class="swiper-slide swiper-no-border">
                    <h1>Diamond ML <b>88rb</b></h1>
                    <div>
                      <img src="{{URL::asset('images/prize-mobile/redeem_mobile_ml88rb.png')}}" alt="pulsa_koin" class="prize-mobile-pulsa_koin"> 
                    </div>
                  </div>
                  <div class="swiper-slide swiper-no-border">
                    <h1>Diamond ML <b>71,5rb</b></h1>
                    <div>
                      <img src="{{URL::asset('images/prize-mobile/redeem_mobile_ml71,5rb.png')}}" alt="pulsa_koin" class="prize-mobile-pulsa_koin"> 
                    </div>
                  </div>
                  <div class="swiper-slide swiper-no-border">
                    <h1>Diamond ML <b>49rb</b></h1>
                    <div>
                      <img src="{{URL::asset('images/prize-mobile/redeem_mobile_ml49rb.png')}}" alt="pulsa_koin" class="prize-mobile-pulsa_koin"> 
                    </div>
                  </div>
                </div>
                <div class="swiper-button-next"></div>
                <div class="swiper-button-prev"></div>
              </div>
            </div>
          </div>
        </div>
      </div>

      {{-- modal tokped--}}
      <div class="modal" id="koin-modal-tokped" tabindex="-1">
        <div class= "modal-dialog modal-dialog-centered" >
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <h1>Koin yang harus ditukarkan</h1>
              <div class="swiper-modal-container">
                <div class="swiper-wrapper">
                  <div class="swiper-slide swiper-no-border">
                    <h1>Voucher Belanja <b>1jt</b></h1>
                    <div>
                      <img src="{{URL::asset('images/prize-mobile/redeem_mobile_tokped1jt.png')}}" alt="pulsa_koin" class="prize-mobile-pulsa_koin"> 
                    </div>
                  </div>
                  <div class="swiper-slide swiper-no-border">
                    <h1>Voucher Belanja <b>500rb</b></h1>
                    <div>
                      <img src="{{URL::asset('images/prize-mobile/redeem_mobile_tokped500rb.png')}}" alt="pulsa_koin" class="prize-mobile-pulsa_koin"> 
                    </div>
                  </div>
                  <div class="swiper-slide swiper-no-border">
                    <h1>Voucher Belanja <b>250rb</b></h1>
                    <div>
                      <img src="{{URL::asset('images/prize-mobile/redeem_mobile_tokped250rb.png')}}" alt="pulsa_koin" class="prize-mobile-pulsa_koin"> 
                    </div>
                  </div>
                  <div class="swiper-slide swiper-no-border">
                    <h1>Voucher Belanja <b>150rb</b></h1>
                    <div>
                      <img src="{{URL::asset('images/prize-mobile/redeem_mobile_tokped150rb.png')}}" alt="pulsa_koin" class="prize-mobile-pulsa_koin"> 
                    </div>
                  </div>
                </div>
                <div class="swiper-button-next"></div>
                <div class="swiper-button-prev"></div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
@endsection

@section('js')


<script src="{{ URL::asset('js/swiper-bundle.min.js') }}"></script>

<script>
  $(document).ready(function() {
    $('#koin-modal-ff').on('shown.bs.modal', function(e) {
      var swiperPrize = new Swiper('.swiper-modal-container', {
      slidesPerView: 1,
      // slidesPerColumn: 1,
      spaceBetween: 10,
      navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
      },
      breakpoints: {
        425: {
          slidesPerView: 1,
          spaceBetween: 10,
        }
      }
    });
    })

    // mole
    $('#koin-modal-ml').on('shown.bs.modal', function(e) {
      var swiperPrizeMole = new Swiper('.swiper-modal-container', {
      slidesPerView: 1,
      // slidesPerColumn: 1,
      spaceBetween: 10,
      navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
      },
      breakpoints: {
        425: {
          slidesPerView: 1,
          spaceBetween: 10,
        }
      }
    });
    })
    $('#koin-modal-tokped').on('shown.bs.modal', function(e) {
      var swiperPrizeTokped = new Swiper('.swiper-modal-container', {
      slidesPerView: 1,
      // slidesPerColumn: 1,
      spaceBetween: 10,
      navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
      },
      breakpoints: {
        425: {
          slidesPerView: 1,
          spaceBetween: 10,
        }
      }
    });
    })
  })
  
</script>

<script>
            
  $('#iphoneButton, #ps5Button, #bikeButton, #cellButton').click(function () {
      var widthWindows = $(window).width();
      // console.log(widthWindows);
      if(widthWindows > 425) {
          $('#iphoneButton, #ps5Button, #bikeButton, #cellButton').removeAttr('data-toggle');
          
          if (this.id == 'iphoneButton') {
              $("#iphoneButton, .prize-iphone").addClass('prize-opacity');
              // $(".prize-iphone").addClass('prize-opacity');
              $(".prize-iphone_koin").css('display', 'block');
              $("#ps5Button, #bikeButton, #cellButton, .prize-ps5, .prize-bike, .prize-pulsa").removeClass('prize-opacity');
              $(".prize-ps5_koin, .prize-bike_koin, .prize-pulsa_koin").css('display', 'none');
          }
          else if (this.id == 'ps5Button') {
              $("#ps5Button, .prize-ps5").addClass('prize-opacity');
              $(".prize-ps5_koin").css('display', 'block');
              $("#iphoneButton, #bikeButton, #cellButton, .prize-iphone, .prize-bike, .prize-pulsa").removeClass('prize-opacity');
              $(".prize-iphone_koin, .prize-bike_koin, .prize-pulsa_koin").css('display', 'none');
          }
          else if (this.id == 'bikeButton') {
              $("#bikeButton, .prize-bike").addClass('prize-opacity');
              $(".prize-bike_koin").css('display', 'block');
              $("#iphoneButton, #ps5Button, #cellButton, .prize-iphone, .prize-ps5, .prize-pulsa").removeClass('prize-opacity');
              $(".prize-iphone_koin, .prize-ps5_koin, .prize-pulsa_koin").css('display', 'none');
          }
          else if (this.id == 'cellButton') {
              $("#cellButton, .prize-pulsa").addClass('prize-opacity');
              $(".prize-pulsa_koin").css('display', 'block');
              $("#iphoneButton, #ps5Button, #bikeButton, .prize-iphone, .prize-ps5, .prize-bike").removeClass('prize-opacity');
              $(".prize-iphone_koin, .prize-ps5_koin, .prize-bike_koin").css('display', 'none');
          }
      }else if (widthWindows <= 425) {
          if (this.id == 'iphoneButton') {
              $('#iphoneButton').attr('data-toggle', 'modal');
          }else if (this.id == 'ps5Button') {
              $('#ps5Button').attr('data-toggle', 'modal');
          }else if (this.id == 'bikeButton') {
              $('#bikeButton').attr('data-toggle', 'modal');
          }else if (this.id == 'cellButton') {
              $('#cellButton').attr('data-toggle', 'modal');
          }
      }
  });
</script>
<script>
  $('#free-fireButton, #moleButton, #tokpedButton').click(function () {
      var widthWindows = $(window).width();

      if(widthWindows > 425) {
        $('#moleButton, #tokpedButton, #free-fireButton').removeAttr('data-toggle');
          if(this.id === 'free-fireButton') {
              $('.table-koin-redeem-1, .table-koin-redeem-2').css('display', 'none');
              $('#moleButton, #tokpedButton').removeClass('btn-prize-red');
              $('.game-cash-prize, .table-koin-redeem').css('display', 'block');
              $('#free-fireButton').addClass('btn-prize-red')
          }else if(this.id === 'moleButton') {
              $('.table-koin-redeem, .table-koin-redeem-2').css('display', 'none');
              $('#free-fireButton, #tokpedButton').removeClass('btn-prize-red');
              $('.game-cash-prize, .table-koin-redeem-1').css('display', 'block');
              $('#moleButton').addClass('btn-prize-red')
          }else if(this.id === 'tokpedButton') {
              $('.table-koin-redeem, .table-koin-redeem-1').css('display', 'none');
              $('#free-fireButton, #moleButton').removeClass('btn-prize-red');
              $('.game-cash-prize, .table-koin-redeem-2').css('display', 'block');
              $('#tokpedButton').addClass('btn-prize-red')
          }
      }else if(widthWindows <= 425) {
          if(this.id === 'free-fireButton') {
            $('#free-fireButton').attr('data-toggle', 'modal');
          }else if(this.id === 'moleButton') {
            $('#moleButton').attr('data-toggle', 'modal');
          }else if(this.id === 'tokpedButton') {
            $('#tokpedButton').attr('data-toggle', 'modal');
          }
      }
      
  })
</script>

    
@endsection
@extends('layouts.app')

@section('content')
    <section class="redeem">
      <div class="redeem-header">
        <h1>Penukaran Koin Gatotkaca</h1>
      </div>
      <div class="container redeem-content">
        <span>Isi Data</span>
        <span>Berikut</span>
        <form action="{{ route('redeemForm') }}" method="post">
          @csrf
          <div class="form-group redeem-category">
            <select name="pilihan" class="form-control redeem-select" id="select-redeem" >
              <option disabled selected>----Pilih Hadiah Redeem----</option>
              <option value="iphone">Iphone</option>
              <option value="ps5">PS5</option>
              <option value="sepeda">Sepeda</option>
              <option value="pulsa">Pulsa</option>
              <option value="ff">Free Fire Diamond</option>
              <option value="mole">Mobile Legends Diamond</option>
              <option value="tokped">Voucher Belanja Tokopedia</option>

            </select>
          </div>
          <div class="form-group redeem-game redeem-form-hide" id="redeem-game-bottom">
            <select name="tipeHadiah" class="form-control redeem-select" id="select-redeem-game" >

            </select>
          </div>
          <div class="koin-requirement">
            <p id=koin-kuantitas>Koin yang dibutuhkan</p>
            <img src="" id="img_koin_redeem" alt="">
          </div>
          <div class="form-group">
            <input type="text" class="form-control redeem-input" id="namaLengkap" name="nama" placeholder="Nama Lengkap" >
          </div>
          <div class="form-group">
            <input type="text" class="form-control redeem-input" id="nomorWa" name="hp" placeholder="Nomor Whatsapp" >
          </div>
          <div class="form-group">
            <input type="email" class="form-control redeem-input" id="alamatEmail" name="email" placeholder="Alamat e-mail" >
          </div>
          <div class="form-group">
            <textarea class="form-control redeem-input" id="alamat-lengkap" rows="6" name="alamat" placeholder="Alamat Lengkap"></textarea>
          </div>
          <button class="btn btn-redeem-form" type="submit">SUBMIT</button>

        </form>
      </div>

      <div class="container-fluid lacak-status-wrapper">
        <form method="post" action="{{ route('redeemTracking') }}">
          @csrf
          <div class="form-group lacak-form">
            <label for="lacakStatus" class="lacak-label">Lacak status redeem anda di sini</label>
            <input type="text" class="form-control input-lacak" id="lacakStatus" name="kode">
            <button class="btn btn-lacak" type="submit">LACAK</button>
          </div>
        </form>
      </div>

      {{-- @if(empty($data))
        <h1>tes1</h1>
      @elseif(!empty($data))
      <h1>tes2</h1>
      @endif --}}
      <div class="progress-bar">
        <ol class="progress-meter">
          <li class="progress-point done">
            <p class="status-category">Verifikasi Redeem<p> 
            <p class="status-detail">verfikasi redeem</p>
          </li>
          <li class="progress-point done">
            <p class="status-category">Verifikasi Redeem<p> 
            <p class="status-detail">verfikasi redeem</p>
          </li>
          <li class="progress-point todo">
            <p class="status-category">Verifikasi Redeem<p> 
            <p class="status-detail">verfikasi redeem</p>
          </li>
        </ol>
      </div>
      
      
    </section>
@endsection


@section('js')
<script>
  var selectOptions = {
      'ff': ['Diamond FF 1jt', 'Diamond FF 500rb', 'Diamond FF 300rb', 'Diamond FF 200rb', 'Diamond FF 100rb', 'Diamond FF 50rb', 'Diamond FF 20rb'],
      'mole' : ['Diamond ML 1jt', 'Diamond ML 500rb', 'Diamond ML 300rb', 'Diamond ML 165rb', 'Diamond ML 120rb', 'Diamond ML 88rb', 'Diamond ML 71,5rb', 'Diamond ML 49rb'],
      'tokped' : ['Voucher Belanja 1jt', 'Voucher Belanja 500rb', 'Voucher Belanja 250rb', 'Voucher Belanja 150rb'],
  };
  $('#select-redeem').on('change', function() {
    var widthWindows = $(window).width();
      var selectValue = $(this).val();
      // alert(selectValue);

      
      if(selectValue === 'iphone') {
          $('#img_koin_redeem').css('display', 'block');
          if (widthWindows > 425){
            var source = "{!! asset('images/koin_redeem_iphone.png') !!}" 
          }else if (widthWindows <= 425) {
            var source = "{!! asset('images/ads/koin_redeem_mobile_iphone.png') !!}" 
          }
          $('#img_koin_redeem').attr('src', source);
          $("#redeem-game-bottom").addClass('redeem-form-hide');
      }else if(selectValue === "ps5") {
          $('#img_koin_redeem').css('display', 'block');
          if (widthWindows > 425){
            var source = "{!! asset('images/koin_redeem_ps5.png') !!}" 
          }else if (widthWindows <= 425) {
            var source = "{!! asset('images/ads/koin_redeem_mobile_ps5.png') !!}" 
          }
          $('#img_koin_redeem').attr('src', source);
          $("#redeem-game-bottom").addClass('redeem-form-hide');
      }else if(selectValue === 'sepeda') {
          $('#img_koin_redeem').css('display', 'block');
          if (widthWindows > 425){
            var source = "{!! asset('images/koin_redeem_sepeda.png') !!}"
          }else if (widthWindows <= 425) {
            var source = "{!! asset('images/ads/koin_redeem_mobile_sepeda.png') !!}"
          }
          $('#img_koin_redeem').attr('src', source);
          $("#redeem-game-bottom").addClass('redeem-form-hide');
      }else if(selectValue === 'pulsa') {
        $('#img_koin_redeem').css('display', 'block');
          if (widthWindows > 425){
            var source = "{!! asset('images/koin_redeem_voucher.png') !!}"
          }else if (widthWindows <= 425) {
            var source = "{!! asset('images/ads/koin_redeem_mobile_pulsa.png') !!}"
          }
          $('#img_koin_redeem').attr('src', source);
          $("#redeem-game-bottom").addClass('redeem-form-hide');
      }else if(selectValue === 'mole') {
          $("#redeem-game-bottom").removeClass('redeem-form-hide');
          $('#select-redeem-game').empty();

          $('#select-redeem-game').append("<option disabled selected >" + "----Pilih Hadiah Redeem----" + "</option>");
          for (i = 0; i < selectOptions[selectValue].length; i++) {
              $('#select-redeem-game').append("<option value='" + selectOptions[selectValue][i] + "'>" + selectOptions[selectValue][i] + "</option>");
          };
          $('#select-redeem-game').on('change', function() {
              var selectValue2 = $(this).val();
              // alert(selectValue2);

              if(selectValue2 === 'Diamond ML 1jt') {
                  $('#img_koin_redeem').css('display', 'block');
                  if (widthWindows > 425){ 
                    var source = "{!! asset('images/redeem_ml1jt.png') !!}"
                  } else if (widthWindows <= 425) {
                    var source = "{!! asset('images/ads/koin_redeem_mobile_ml1jt.png') !!}"
                  }
                  $('#img_koin_redeem').attr('src', source);
              }else if(selectValue2 === 'Diamond ML 500rb') {
                  $('#img_koin_redeem').css('display', 'block');
                  if (widthWindows > 425){ 
                    var source = "{!! asset('images/redeem_ml500rb.png') !!}"
                  } else if (widthWindows <= 425) {
                    var source = "{!! asset('images/ads/koin_redeem_mobile_ml500rb.png') !!}"
                  }
                  $('#img_koin_redeem').attr('src', source);
              }else if(selectValue2 === 'Diamond ML 300rb') {
                  $('#img_koin_redeem').css('display', 'block');
                  if (widthWindows > 425){ 
                    var source = "{!! asset('images/redeem_ml300rb.png') !!}"
                  } else if (widthWindows <= 425) {
                    var source = "{!! asset('images/ads/koin_redeem_mobile_ml300rb.png') !!}"
                  }
                  $('#img_koin_redeem').attr('src', source);
              }else if(selectValue2 === 'Diamond ML 165rb') {
                  $('#img_koin_redeem').css('display', 'block');
                  if (widthWindows > 425){ 
                    var source = "{!! asset('images/redeem_ml165rb.png') !!}"
                  } else if (widthWindows <= 425) {
                    var source = "{!! asset('images/ads/koin_redeem_mobile_ml165rb.png') !!}"
                  }
                  $('#img_koin_redeem').attr('src', source);
              }else if(selectValue2 === 'Diamond ML 120rb') {
                  $('#img_koin_redeem').css('display', 'block');
                  if (widthWindows > 425){ 
                    var source = "{!! asset('images/redeem_ml120rb.png') !!}"
                  } else if (widthWindows <= 425) {
                    var source = "{!! asset('images/ads/koin_redeem_mobile_ml120rb.png') !!}"
                  }
                  $('#img_koin_redeem').attr('src', source);
              }else if(selectValue2 === 'Diamond ML 88rb') {
                  $('#img_koin_redeem').css('display', 'block');
                  if (widthWindows > 425){ 
                    var source = "{!! asset('images/redeem_ml88rb.png') !!}"
                  } else if (widthWindows <= 425) {
                    var source = "{!! asset('images/ads/koin_redeem_mobile_ml88rb.png') !!}"
                  }
                  $('#img_koin_redeem').attr('src', source);
              }else if(selectValue2 === 'Diamond ML 71,5rb') {
                  $('#img_koin_redeem').css('display', 'block');
                  if (widthWindows > 425){ 
                    var source = "{!! asset('images/redeem_ml71,5rb.png') !!}"
                  } else if (widthWindows <= 425) {
                    var source = "{!! asset('images/ads/koin_redeem_mobile_ml71,5rb.png') !!}"
                  }
                  $('#img_koin_redeem').attr('src', source);
              }else if(selectValue2 === 'Diamond ML 49rb') {
                  $('#img_koin_redeem').css('display', 'block');
                  if (widthWindows > 425){ 
                    var source = "{!! asset('images/redeem_ml49rb.png') !!}"
                  } else if (widthWindows <= 425) {
                    var source = "{!! asset('images/ads/koin_redeem_mobile_ml49rb.png') !!}"
                  }
                  $('#img_koin_redeem').attr('src', source);
              }
          })
      } else if(selectValue === 'ff') {
          $("#redeem-game-bottom").removeClass('redeem-form-hide');
          $('#select-redeem-game').empty();
          $('#img_koin_redeem').css('display', 'none');

          $('#select-redeem-game').append("<option disabled selected >" + "----Pilih Hadiah Redeem----" + "</option>");
          for (i = 0; i < selectOptions[selectValue].length; i++) {
              $('#select-redeem-game').append("<option value='" + selectOptions[selectValue][i] + "'>" + selectOptions[selectValue][i] + "</option>");
          };
          $('#select-redeem-game').on('change', function() {
              var selectValue2 = $(this).val();
              // alert(selectValue2);

              if(selectValue2 === 'Diamond FF 1jt') {
                  $('#img_koin_redeem').css('display', 'block');
                  if (widthWindows > 425){ 
                    var source = "{!! asset('images/redeem_ff1jt.png') !!}"
                  } else if (widthWindows <= 425) {
                    var source = "{!! asset('images/ads/koin_redeem_mobile_ff1jt.png') !!}"
                  }
                  $('#img_koin_redeem').attr('src', source);
              }else if(selectValue2 === 'Diamond FF 500rb') {
                  $('#img_koin_redeem').css('display', 'block');
                  if (widthWindows > 425){ 
                    var source = "{!! asset('images/redeem_ff500rb.png') !!}"
                  } else if (widthWindows <= 425) {
                    var source = "{!! asset('images/ads/koin_redeem_mobile_ff500rb.png') !!}"
                  }
                  $('#img_koin_redeem').attr('src', source);
              }else if(selectValue2 === 'Diamond FF 300rb') {
                  $('#img_koin_redeem').css('display', 'block');
                  if (widthWindows > 425){ 
                    var source = "{!! asset('images/redeem_ff300rb.png') !!}"
                  } else if (widthWindows <= 425) {
                    var source = "{!! asset('images/ads/koin_redeem_mobile_ff300rb.png') !!}"
                  }
                  $('#img_koin_redeem').attr('src', source);
              }else if(selectValue2 === 'Diamond FF 200rb') {
                  $('#img_koin_redeem').css('display', 'block');
                  if (widthWindows > 425){ 
                    var source = "{!! asset('images/redeem_ff200rb.png') !!}"
                  } else if (widthWindows <= 425) {
                    var source = "{!! asset('images/ads/koin_redeem_mobile_ff200rb.png') !!}"
                  }
                  $('#img_koin_redeem').attr('src', source);
              }else if(selectValue2 === 'Diamond FF 100rb') {
                  $('#img_koin_redeem').css('display', 'block');
                  if (widthWindows > 425){ 
                    var source = "{!! asset('images/redeem_ff100rb.png') !!}"
                  } else if (widthWindows <= 425) {
                    var source = "{!! asset('images/ads/koin_redeem_mobile_ff100rb.png') !!}"
                  }
                  $('#img_koin_redeem').attr('src', source);
              }else if(selectValue2 === 'Diamond FF 50rb') {
                  $('#img_koin_redeem').css('display', 'block');
                  if (widthWindows > 425){ 
                    var source = "{!! asset('images/redeem_ff50rb.png') !!}"
                  } else if (widthWindows <= 425) {
                    var source = "{!! asset('images/ads/koin_redeem_mobile_ff50rb.png') !!}"
                  }
                  $('#img_koin_redeem').attr('src', source);
              }else if(selectValue2 === 'Diamond FF 20rb') {
                  $('#img_koin_redeem').css('display', 'block');
                  if (widthWindows > 425){ 
                    var source = "{!! asset('images/redeem_ff20rb.png') !!}"
                  } else if (widthWindows <= 425) {
                    var source = "{!! asset('images/ads/koin_redeem_mobile_ff20rb.png') !!}"
                  }
                  $('#img_koin_redeem').attr('src', source);
              }
          })
      }else if(selectValue === 'tokped') {
          $("#redeem-game-bottom").removeClass('redeem-form-hide');
          $('#select-redeem-game').empty();
          $('#img_koin_redeem').css('display', 'none');

          $('#select-redeem-game').append("<option disabled selected >" + "----Pilih Hadiah Redeem----" + "</option>");
          for (i = 0; i < selectOptions[selectValue].length; i++) {
              $('#select-redeem-game').append("<option value='" + selectOptions[selectValue][i] + "'>" + selectOptions[selectValue][i] + "</option>");
          };
          $('#select-redeem-game').on('change', function() {
              var selectValue2 = $(this).val();
              // alert(selectValue2);

              if(selectValue2 === 'Voucher Belanja 1jt') {
                  $('#img_koin_redeem').css('display', 'block');
                  if (widthWindows > 425){ 
                    var source = "{!! asset('images/redeem_tokped1jt.png') !!}"
                  } else if (widthWindows <= 425) {
                    var source = "{!! asset('images/ads/koin_redeem_mobile_tokped1jt.png') !!}"
                  }
                  $('#img_koin_redeem').attr('src', source);
              }else if(selectValue2 === 'Voucher Belanja 500rb') {
                  $('#img_koin_redeem').css('display', 'block');
                  if (widthWindows > 425){ 
                    var source = "{!! asset('images/redeem_tokped500rb.png') !!}"
                  } else if (widthWindows <= 425) {
                    var source = "{!! asset('images/ads/koin_redeem_mobile_tokped500rb.png') !!}"
                  }
                  $('#img_koin_redeem').attr('src', source);
              }else if(selectValue2 === 'Voucher Belanja 250rb') {
                  $('#img_koin_redeem').css('display', 'block');
                  if (widthWindows > 425){ 
                    var source = "{!! asset('images/redeem_tokped250rb.png') !!}"
                  } else if (widthWindows <= 425) {
                    var source = "{!! asset('images/ads/koin_redeem_mobile_tokped250rb.png') !!}"
                  }
                  $('#img_koin_redeem').attr('src', source);
              }else if(selectValue2 === 'Voucher Belanja 150rb') {
                  $('#img_koin_redeem').css('display', 'block');
                  if (widthWindows > 425){ 
                    var source = "{!! asset('images/redeem_tokped150rb.png') !!}"
                  } else if (widthWindows <= 425) {
                    var source = "{!! asset('images/ads/koin_redeem_mobile_tokped150rb.png') !!}"
                  }
                  $('#img_koin_redeem').attr('src', source);
              }
          })
      }
  })
</script>
    
@endsection
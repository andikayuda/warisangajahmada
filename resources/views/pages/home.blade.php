@extends('layouts.app')

@section('content')
    <div>
        {{-- BANNER SECTION --}}
        <div class="hero">
            <div class="marquee">
                <div class="hero-txt">
                    <h1>Warisan Gajahmada lagi bagi-bagi hadiah nih!</h1> <br>
                    <a href="{{ route('prize') }}" class="hero-btn">Pelajari Lebih Lanjut</a>
                </div>
                <div class="track">
                    <img class="marquee-img" src="{{ url('./images/marquee/ps5.png') }}" alt="">
                    <img class="marquee-img" src="{{ url('./images/marquee/sepeda.png') }}" alt="">
                    <img class="marquee-img" src="{{ url('./images/marquee/iphone.png') }}" alt="">
                    <img class="marquee-img" src="{{ url('./images/marquee/ps5.png') }}" alt="">
                    <img class="marquee-img" src="{{ url('./images/marquee/sepeda.png') }}" alt="">
                    <img class="marquee-img" src="{{ url('./images/marquee/iphone.png') }}" alt="">
                </div>
            </div>
        </div>

        {{-- PRODUCT SECTION --}}
        <div class="produk">
            <h1><span>Produk</span> Warisan Gajahmada</h1>
            <div class="produk-card container">

                <div class="produk-search">
                    <input class="form-control input-search" type="text" placeholder="Search...">
                </div>

                

                <div class="row rowgap">
                    <div class="col-6 col-sm-6 col-md-4 ">
                        <div class="card produk-card-item">
                            <div class="card-body">
                                <img src="{{ url('./images/produk-1.png') }}" class="produk-card-img" alt="">
                                <p>Larutan Penyegar Cap Badak <br> Paket 6pcs - Leci <br> (Edisi Gatotkaca + Koin)</p>
                                <h5>Rp. 48.500,-</h5>
                                <button class="produk-card-btn">Pesan Sekarang</button>
                            </div>
                        </div>
                    </div>
                    <div class="col-6 col-sm-6 col-md-4 ">
                        <div class="card produk-card-item">
                            <div class="card-body">
                                <img src="{{ url('./images/produk-1.png') }}" class="produk-card-img" alt="">
                                <p>Larutan Penyegar Cap Badak <br> Paket 6pcs - Leci <br> (Edisi Gatotkaca + Koin)</p>
                                <h5>Rp. 48.500,-</h5>
                                <button class="produk-card-btn">Pesan Sekarang</button>
                            </div>
                        </div>
                    </div>
                    <div class="col-6 col-sm-6 col-md-4 ">
                        <div class="card produk-card-item">
                            <div class="card-body">
                                <img src="{{ url('./images/produk-1.png') }}" class="produk-card-img" alt="">
                                <p>Larutan Penyegar Cap Badak <br> Paket 6pcs - Leci <br> (Edisi Gatotkaca + Koin)</p>
                                <h5>Rp. 48.500,-</h5>
                                <button class="produk-card-btn">Pesan Sekarang</button>
                            </div>
                        </div>
                    </div>
                    <div class="col-6 col-sm-6 col-md-4 ">
                        <div class="card produk-card-item">
                            <div class="card-body">
                                <img src="{{ url('./images/produk-1.png') }}" class="produk-card-img" alt="">
                                <p>Larutan Penyegar Cap Badak <br> Paket 6pcs - Leci <br> (Edisi Gatotkaca + Koin)</p>
                                <h5>Rp. 48.500,-</h5>
                                <button class="produk-card-btn">Pesan Sekarang</button>
                            </div>
                        </div>
                    </div>
                    <div class="col-6 col-sm-6 col-md-4 ">
                        <div class="card produk-card-item">
                            <div class="card-body">
                                <img src="{{ url('./images/produk-1.png') }}" class="produk-card-img" alt="">
                                <p>Larutan Penyegar Cap Badak <br> Paket 6pcs - Leci <br> (Edisi Gatotkaca + Koin)</p>
                                <h5>Rp. 48.500,-</h5>
                                <button class="produk-card-btn">Pesan Sekarang</button>
                            </div>
                        </div>
                    </div>
                </div>

               
            </div>
        </div>

        {{-- PROMOSI SECTION --}}
        <div class="promosi container">
            <h1>Promosi</h1>
            <div class="bagi-hadiah">
                <p>KV UNTUK BAGI BAGI HADIAH</p>
            </div>

            <div class="row tukar-koin" style="row-gap: 20px">
                
                <div class="col-md-6">
                    <div class="card promosi-card">
                        <div class="card-body">
                          <h4>Tukar Koin</h4>
                          <select class="form-select tukar-koin-select" aria-placeholder="----Pilih Hadiah Redeem----">
                            <option value="0">----Pilih Hadiah Redeem----</option>
                            <option value="1">One</option>
                            <option value="2">Two</option>
                            <option value="3">Three</option>
                          </select>
                          <button class="lacak-btn">TUKAR</button>
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="card promosi-card">
                        <div class="card-body">
                          <h4>Lacak Status</h4>
                          <input class="form-control axd" type="text" placeholder="Kode Redeem">
                          <button class="lacak-btn">LACAK</button>  
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection
@extends('layouts.app')

@section('content')

<div class="container">
    <h3 class="mt-5">Keranjang</h3>
    <div class="cart">
        <div class="card">
            <div class="card-body cart-item">
                <img src="{{asset('images/bundle.png')}}" class="cart-img" alt="product" >
                <div class="item-desc">
                    <h4>Larutan Penyegar Cap Badak - Bundle 6 <br>
                        Rasa Jambu</h4>
                    <h2>Rp52.423,-</h2>
                    <a href="#">Remove</a>
                </div>
                <div class="item-qty">
                    <i class="fas fa-plus"></i>
                    <h3 class="mt-2">1</h3>
                    <i class="fas fa-minus"></i>
                </div>
            </div>
        </div>
        <div class="card">
            <div class="card-body cart-item">
                <img src="{{asset('images/bundle.png')}}" class="cart-img" alt="product" >
                <div class="item-desc">
                    <h4>Larutan Penyegar Cap Badak - Bundle 6 <br>
                        Rasa Jambu</h4>
                    <h2>Rp52.423,-</h2>
                    <a href="#">Remove</a>
                </div>
                <div class="item-qty">
                    <i class="fas fa-plus"></i>
                    <h3 class="mt-2">1</h3>
                    <i class="fas fa-minus"></i>
                </div>
            </div>
        </div>
        
    </div>

    <div class="coupon">
        <div class="card">
            <div class="card-body">
              <h4>Masukkan Kode Kupon</h4>
              {{-- <input class="form-control" type="text" placeholder="Kode Kupon" aria-label="default input example"> --}}
              <div class="input-group cek-coupon">
                <input type="text" class="form-control" placeholder="Kode Kupon">
                <button class="cek-coupon-btn" type="button">CEK</button>
              </div>

              <div class="row" style="margin-top: 25px">
                  <div class="col-md-6">
                      <div class="coupon-subtotal my-3"> 
                          <h4>PPN 10%</h4>
                          <h4>Rp 142.200</h4>
                      </div>
                      <div class="coupon-subtotal"> 
                        <h3>Total</h3>
                        <h3>Rp 122.200</h3>
                    </div>
                  </div>
                  <div class="col-md-6 btn-checkout-container">
                      <button class="btn-checkout">CHECKOUT</button>
                  </div>
              </div>
            </div>
          </div>
    </div>
    
</div>




{{-- <div class="container">
    <div class="cart">
        <h4 class="cart-title">Keranjang</h4>
        @for($i=0;$i<=1;$i++)
        <div class="row cart-box">
            <div class="col-md-3 col-4">
                <img src="{{asset('images/bundle.png')}}" class="img-fluid" alt="" width="200" height="200">
            </div>
            <div class="col-md-7 col-6 align-self-center text-left">
                <h5 class="product-title">Larutan Penyegar Cap Badak - Bundle 6</h5>
                <span class="product-taste">Rasa Jambu - Rp 48.500,-</span>
                <p class="product-price" id="price{{$i}}">48.500</p>
                <span class="remove">Remove</span>
            </div>
            <div class="col-md-2 col-2 align-self-center text-center">
                <div class="quantity">
                    <button class="plus-btn" type="button" name="button" id="plusButton{{$i}}" >
                        <img src="{{asset('images/plus.svg')}}" alt="" />
                    </button><br>
                    <input type="text" name="name" value="1" id="{{$i}}"><br>
                    <button class="minus-btn opacity-30" type="button" id="minusButton{{$i}}" name="button">
                        <img src="{{asset('images/minus.svg')}}" alt="" />
                    </button>
                </div>
            </div>
       </div>
    @endfor
    </div>
    <div class="checkout">
        <p class="voucher-title">Masukkan Kode Kupon</p>
        <div class="row">
            <div class="col-md-5">
            <div class="input-group mb-3">
                <input type="text" class="form-control" placeholder="Kode kupon" aria-label="Kode kupon" aria-describedby="basic-addon2">
                <div class="input-group-append">
                    <button class="btn btn-danger check-button" type="button">CEK</button>
                </div>
            </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-5">
                <div class="row">
                    <div class="col-6 text-left">
                        <h4 class="ppn">PPN 10%</h4>
                    </div>
                    <div class="col-6 text-right">
                        <p class="ppn-value">Rp 142.200</p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-4 text-left">
                        <h4 class="total">Total</h4>
                    </div>
                    <div class="col-8 text-right">
                        <p class="total-value">Rp 1.564.200</p>
                    </div>
                </div>
            </div>
            <div class="col-md-7 text-center align-self-center">
                <button width="100" class="btn btn-danger checkout-button"> Checkout</button>
            </div>
        </div>

    </div>

</div>
@endsection

@section('js')
<script>
    // -----------------for-Shopping-cart-------------


    $('.minus-btn').on('click', function(e) {
    e.preventDefault();
    var $this = $(this);
    var $input = $this.closest('div').find('input');
    var value = parseInt($input.val());
    // var price = $this.closest('div').find('.product-prize').value;
    // console.log(price);
    if(value == 1){
        value = 1;
    }
    else{
        value = value - 1;
    }
  $input.val(value);
//   console.log("value"+ value)
    var id = $input.attr('id');
    var id = $input.attr('id');
    var id = 'minusButton'+id;
    // console.log(id);
    checkClass(value, id);
});
 
$('.plus-btn').on('click', function(e) {
    e.preventDefault();
    var $this = $(this);
    var $input = $this.closest('div').find('input');
    var value = parseInt($input.val());
    // var price = $this.document.getElementsByClassName('product-prize');
    // console.log(price);
    value = value + 1;

    $input.val(value);
    var id = $input.attr('id');
    var id = 'minusButton'+id;
    // console.log(id);
    checkClass(value, id);
    // console.log("value"+ value)
});

function checkClass(value, id){
    console.log(value);
    console.log(id);
    // var input = document.getElementById(`${id}`).value;
    // var input = document.getElementById('0');
    // console.log(input);
    if(value <= 1 ){
        $(`#${id}`).addClass('opacity-30');
       
    }
    else{
        $(`#${id}`).removeClass('opacity-30');
    }
}

function changePrice(value, id){
    var data = value * id;
    return data;
}

</script> --}}
@endsection
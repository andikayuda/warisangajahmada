<div class="footer">
    <div class="row" style="row-gap: 20px"> 
        
        <div class="col-md-6">
            <div class="footer-left">
                <img src="{{ url('./images/footer-logo.png') }}" alt="" class="img-fluid">
                <ul>
                    <li><a href="{{ route('home') }}">Home</a></li>
                    <li><a href="{{ route('prize') }}">Prize</a></li>
                    <li><a href="{{ route('koin') }}">Koin Gatotkaca</a></li>
                    <li><a href="{{ route('redeem') }}">Tukar Koin</a></li>
                    <li><a href="{{ route('home') }}">Cart</a></li>
                </ul>
            </div>
        </div>

        <div class="col-md-6">
            <h5 style="color: white">Temukan kami di</h5>
            <div class="social-button">
                <a href="https://www.tokopedia.com/warisangajahmada" class="tokped-btn"><img class="tokped-img" src="{{ url('./images/tokped.svg') }}" alt="">Tokopedia</a>
                <a href="https://shopee.co.id/warisangajahmada" class="sopi-btn"><img class="sopi-img" src="{{ url('./images/sopi.svg') }}" alt=""> Shopee</a>
                <a href="https://www.blibli.com/merchant/warisan-gajahmada/WAG-60046?page=1&start=0&pickupPointCode=&cnc=&multiCategory=true&excludeProductList=true&promoTab=false&sort=7" class="blibli-btn"><img class="blibli-img" src="{{ url('./images/blibli.svg') }}" alt=""> Blibli</a>
            </div>
            <h5 class="mt-3" style="color: white">Kontak Kami</h5>
            <a href="#" class="wa-btn"><img class="mr-2" src="{{ url('./images/wa.svg') }}" alt="">Whatsapp Business</a>
        </div>

    </div>
    <div class="divider"></div>
    <p style="text-align: right; color:white">Copyright Warisan Gajahmada - All Right Reserved</p>        
</div>

<div class="footer-mobile-2">
    <div class="row">
        <h5 style="color: white">Temukan kami di</h5>
        <div class="social-button-2">
            <a href="https://www.tokopedia.com/warisangajahmada" class="tokped-btn-2"><img class="tokped-img-2" src="{{ url('./images/tokped.svg') }}" alt="">Tokopedia</a>
            <a href="https://shopee.co.id/warisangajahmada" class="sopi-btn-2"><img class="sopi-img-2" src="{{ url('./images/sopi.svg') }}" alt=""> Shopee</a>
            <a href="https://www.blibli.com/merchant/warisan-gajahmada/WAG-60046?page=1&start=0&pickupPointCode=&cnc=&multiCategory=true&excludeProductList=true&promoTab=false&sort=7" class="blibli-btn-2"><img class="blibli-img-2" src="{{ url('./images/blibli.svg') }}" alt=""> Blibli</a>
        </div>
        <h5 class="mt-3" style="color: white">Kontak Kami</h5>
        <a href="#" class="wa-btn-2"><img class="mr-2" src="{{ url('./images/wa.svg') }}" alt="">Whatsapp Business</a>
    </div>
    <div class="divider"></div>
    <div class="row">
        <div class="col-md-6">
            <div class="footer-left-2">
                <img src="{{ url('./images/footer-logo.png') }}" alt="" class="img-fluid">
                <p style="text-align: right; color:white">Copyright Warisan Gajahmada - All Right Reserved</p>        
            </div>
        </div>
    </div>
</div>
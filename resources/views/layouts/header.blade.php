<nav class="navbar navbar-expand-lg">
    <a class="navbar-brand" href="{{ route('home') }}">
        <img src="{{ url('./images/logo.svg') }}" alt="">
    </a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <i class="fas fa-bars burger-btn"></i>
    </button>
  
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav ml-auto">
        <li class="nav-item active">
          <a class="nav-link" href="{{ route('home') }}">Home <span class="sr-only">(current)</span></a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="{{ route('koin') }}">Koin Gatotkaca</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="{{ route('redeem') }}">Tukar Koin</a>
        </li>
        <li class="nav-item">
          <a class="nav-link wa-b" href="#">Whatsapp Business</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="{{ route('cart') }}"><i class="fas fa-shopping-cart"></i></a>
        </li>
    
      </ul>
    </div>
  </nav>
<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Warisan Gajahmada</title>

        {{-- Bootstrap --}}
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
             
        <!-- CSS -->
        <link rel="stylesheet" href="{{URL::to('css/styles.css')}}">

        {{-- Font Awesome --}}
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.2/css/all.min.css">

        {{-- swiper css --}}
        <link rel="stylesheet" href="{{ URL::asset('css/swiper-bundle.min.css') }}">

        <!-- Styles -->
        <meta name="csrf-token" content="{{ csrf_token() }}">
    </head>
    <body>
        @include('layouts.header')
        @yield('content')
        @include('layouts.footer2')
        
        <script src="{{ URL::asset('js/jquery-3.4.1.min.js') }}"></script>
        <script
            src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"
            integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV"
            crossorigin="anonymous"
        ></script>
        @yield('js')

        
    </body>
</html>

<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('/', 'PagesController@home')->name('home');
Route::get('/prize', 'PagesController@prize')->name('prize');
Route::get('/redeem', 'PagesController@redeem')->name('redeem');
Route::get('/ads/{slug}', 'PagesController@ads')->name('ads');
// Route::post('/lacak', 'PagesController@lacak')->name('lacak');
Route::post('/redeem/tracking','PagesController@redeemTracking')->name('redeemTracking');
Route::post('/redeem-form', 'PagesController@redeemForm')->name('redeemForm');
Route::get('/redeem/tracker/{kode}', 'PagesController@redeemTracker')->name('redeemTracker');
Route::get('/koin-gatotkaca', 'PagesController@koin')->name('koin');
Route::get('/cart', 'PagesController@cart')->name('cart');
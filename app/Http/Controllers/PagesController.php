<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Jenssegers\Agent\Agent;

class PagesController extends Controller
{
    public function home()
    {
        return view('pages.home');
    }
    public function prize() {
        return view('pages.prize');
    }

    public function redeem() {
        
        return view('pages.redeem');
    }

    
    // public function lacak(Request $request) {
    //     dd($request);
    // }

    public function cart(){
        return view('pages.cart');
    }
    public function redeemForm(Request $request) {
        // dd($request);
        $kodeRedeem = str_random(6);
      $kodeRedeem = mb_strtoupper($kodeRedeem);

      $whatsapp = $request->phone;
    $whatsapp = str_replace('-','',$whatsapp);
    $whatsapp = str_replace(' ','',$whatsapp);
    $whatsapp = str_replace(' ','',$whatsapp);
    $whatsapp = str_replace('.','',$whatsapp);
    $whatsapp = preg_replace("/[^0-9.]/", "", $whatsapp);
    $check_number = str_split($whatsapp);
    $new_number = "62";

    if($check_number[0]=='0'){
      foreach($check_number as $n => $number){
        if($n > 0){
          if($check_number[1]=='8'){
            $new_number .= $number;
          }else{
            //Alert::success('Silahkan coba lagi','Nomor Tidak Valid '.$whatsapp);
            //Return redirect()->back();
            //echo "ga bisa dibenerin nomor $isi->id <br>";
            $new_number = '-';

          }
        }
      }
    }else{
      if($check_number[0]=='8'){
        $new_number = "62".$whatsapp;
      }elseif($check_number[0]=='6'){
        $new_number = $whatsapp;
      }elseif($check_number[0]=='+'){
        foreach($check_number as $n => $number){
          if($n > 2){
              $new_number .= $number;
          }
        }
      }else{
        //Alert::success('Silahkan coba lagi','Nomor Tidak Valid '.$whatsapp);
        //Return redirect()->back();
        $new_number = '-';
        //echo "ga bisa dibenerin  $isi->id <br>";

      }
    }


    if($request->tipeHadiah){
        $insert = \DB::connection('koin')->table('redeem')->insertGetId([
            'kodeRedeem' => $kodeRedeem,
            'nama' => $request->nama,
            'noHandphone' => $request->hp,
            'jenisHadiah' => $request->pilihan,
            'tipeHadiah' => $request->tipeHadiah,
            'email' => $request->email,
            'noWhatsapp' => $new_number,
            'alamat' => $request->alamat,
            'tanggalStatusRedeem' => date('Y-m-d H:i:s')
          ]);
    } else {
        $insert = \DB::connection('koin')->table('redeem')->insertGetId([
            'kodeRedeem' => $kodeRedeem,
            'nama' => $request->nama,
            'noHandphone' => $request->hp,
            'jenisHadiah' => $request->pilihan,
            // 'tipeHadiah' => $request->tipeHadiah,
            'email' => $request->email,
            'noWhatsapp' => $new_number,
            'alamat' => $request->alamat,
            'tanggalStatusRedeem' => date('Y-m-d H:i:s')
          ]);
    }
      



    //   $client = new Client();
/*
      $res = $client->post(env('CHAT-API-PATH').'sendMessage?token='.env('CHAT-API-TOKEN'),[
          'form_params' => [
            "phone" => $new_number,
            "body" => "Hi $request->nama, kode redeem kamu adalah $kodeRedeem,

silahkan cek secara berkala di https://koin.warisangajahmada.com/"
          ]
        ]);
*/
      return redirect()->route('redeemTracker',$kodeRedeem);
        // dd($request);
    }

    public function redeemTracker($kode){
        // dd($kode);
        $data = \DB::connection('koin')->table('redeem')->where('kodeRedeem',$kode)->first();
        // $prizes = \DB::table('prizes')->where('statusHadiah','active')->get();
        dd($data);
        // return view('pages.redeem',compact('data','prizes'));
        return view('pages.redeem',compact('data'));
    }
    
    public function ads($slug) {
        $agent = new Agent();
        \Session::put('isMobile', $agent->isMobile());
        $data=\DB::table('affinity')->where('slug', $slug)->first();
        return view('pages.ads',compact('data'));
    }

    public function redeemTracking(Request $request){
        // dd($request);
        return redirect()->route('redeemTracker',$request->kode);
    }

    public function koin()
    {
        return view('pages.koin');
    }
}
